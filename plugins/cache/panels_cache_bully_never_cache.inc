<?php

/**
 * @file
 * Provides no-caching mechanism for Panels cache bully.
 */

// Plugin definition.
$plugin = array(
  'title' => t('No panels cache bully'),
  'description' => t('Panels cache bully "never cache" option.'),
  'defaults' => '',
  'preview' => 'none',
  'icon' => 'none',
);
