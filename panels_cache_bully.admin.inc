<?php

/**
 * @file
 * Administration form.
 */

/**
 * Page callback for settings form.
 *
 * @see: panels_cache_bully_menu().
 */
function panels_cache_bully_settings_form($form, &$form_state) {
  $settings = panels_cache_bully_get_settings();

  $form['panels_cache_bully_cache_plugin'] = array('#tree' => TRUE);

  $form['panels_cache_bully_cache_plugin']['method'] = array(
    '#type' => 'radios',
    '#title' => t('Panels cache plugin to use'),
    '#default_value' => $settings['method'],
    '#description' => t('The default settings of the selected cache plugin will be used by Panels cache bully.'),
    '#options' => panels_cache_bully_plugins_options_list(),
    '#required' => TRUE,
  );

  $form['panels_cache_bully_cache_plugin']['settings'] = array(
    '#type' => 'vertical_tabs',
    '#title' => t('Plugin-specific settings.'),
    '#default_tab' => 'edit-panels-cache-bully-cache-plugin-settings-' . drupal_clean_css_identifier($settings['method']),
  );

  $panels_cache_plugins = ctools_get_plugins('panels', 'cache');
  $fake_display = _panels_cache_bully_get_fake_display();
  foreach ($panels_cache_plugins as $plugin => $plugin_settings) {
    if (isset($plugin_settings['settings form'])) {
      $subform = $plugin_settings['settings form']($settings['settings'][$plugin], $fake_display, 0);
      $subform['#type'] = 'fieldset';
      $subform['#title'] = $plugin_settings['title'];
      $form['panels_cache_bully_cache_plugin']['settings'][$plugin] = $subform;
    }
  }

  return system_settings_form($form);
}

/**
 * Returns options list with Panels cache plugins.
 *
 * The default settings of each plugin are printed in json format.
 */
function panels_cache_bully_plugins_options_list() {
  $panels_cache_plugins = ctools_get_plugins('panels', 'cache');

  $options = array();
  foreach ($panels_cache_plugins as $plugin => $settings) {
    $options[$plugin] = $settings['title'];
  }
  return $options;
}

/**
 * Generates a fake panels display object to pass to a panel form builder.
 *
 * @return object
 *   An empty panels display object.
 *
 * @see panels_load_display
 */
function _panels_cache_bully_get_fake_display() {
  return (object) array(
    'did' => 0,
    'name' => '',
    'layout' => 'none',
    'panel_settings' => NULL,
    'layout_settings' => NULL,
    'css_id' => NULL,
    'content' => array(),
    'panels' => array(),
    'cache' => array(),
    'args' => array(),
    'incoming_content' => array(),
    'context' => array(),
  );
}
